# Analyze.js

Slides are in the PDF  
The available search engines are: Google, Bing, Yahoo!, Yandex, DuckDuckGo.  


**Summary:**  
_ Install node.js and check with "node -v"  
__ https://nodejs.org/en  

_ Download and install Visual Studio Code  
__ https://code.visualstudio.com/  

_ Download workshop code from gitlab:  
__ https://gitlab.com/personal1285309/PCD2023_Renee_WorkshopSoftware  

__ Extract the code and open code folder in Visual Studio Code. Also, start terminal in code folder. (This is different on diff OS. Win uses git bash -> install git for windows)  

_ Install dependencies: "npm install"  

_ Saving web pages: Type "Postdigital" into Google Search and use dev tools to extract outer html from Elements (/Inspector) tab.  

_ Use to code (analyse.js) to analyze html files and extract titles and links from pages  

_ Download and install Processing.  
__ https://processing.org/  
__ Locate the Processing folder, and double click processing.exe inside of it.  
__ Now load renee2.pde from the file menu.  

_ Upload your results:  
__ https://cloud.nubo.coop/s/HCQHbkpNw5jYDCP?path=%2FHASTAC%2FRen%C3%A9e  

_ Interpret your results  