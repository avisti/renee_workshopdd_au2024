const cheerio = require("cheerio");
const fs = require('fs')
const chalk = require('chalk');

// Make file paths global
const basePath = "savedWebPages/";
let parsedResults = []; // for titles, links and snippets
let justUrlResults = []; // just for links
const urlOutputDir = "./htmlLinks/";
const dataOutputDir = "./htmlData/";
let $;


///////////////////////////////////////////////////
// analyze downloaded Google files with cheerio
const analyze = (searchEngine, file) => {

  console.log("searchEngine:", searchEngine);
  console.log("file:", file);

  let titles = [];
  let links = [];
  let snippets = [];

  const fileName = file.replace(".", "-");


  ////////////////////////////////////////////////////////////////
  // CASE GOOGLE:
  if (searchEngine.toLowerCase() == "google") {
    
    // load cheerio file
    fs.readFile(basePath + file, 'utf8', (err, data) => {

      if (err) throw err;
      $ = cheerio.load(data);
      // console.log($.html());
        
      // Using Cheerio to find elements in page
      $(".yuRUbf a h3").each((i, el) => {
        titles[i] = $(el).text();
      });
      $(".yuRUbf a").each((i, el) => {
        links[i] = $(el).attr("href");
      });
      $(".VwiC3b.yXK7lf.yDYNvb.W8l4ac.lyLwlc.lEBKkf").each((i, el) => {
        snippets[i] = $(el).text();
      });
      // console.log("titles:", titles);
      // console.log("links:", links);
      // console.log("snippets:", snippets);

      for (let i = 0; i < titles.length; i++) {
        parsedResults.push({
          count: i,
          title: titles[i],
          links: links[i],
          snippet: snippets[i]
        });
        justUrlResults.push(links[i])
      }
      // writing results to files
      exportResults(parsedResults, justUrlResults, fileName); // "Google");
    });
  }


  ////////////////////////////////////////////////////////////////
  // CASE DuckDuckGo:
  if (searchEngine.toLowerCase() == "duckduckgo") {

    // ethvert resultat er pakket ind i klassen .wLL07_0Xnd1QZpzpfR4W
    // derinde i: .ikg2IXiCD14iVX7AdZo1 h2 a span

    fs.readFile(basePath + file, 'utf8', (err, data) => {

      if (err) throw err;
      $ = cheerio.load(data);
          
      // Using Cheerio to find elements in page
      $(".ikg2IXiCD14iVX7AdZo1 h2 a span").each((i, el) => {
        titles[i] = $(el).text();
      });  
      $(".ikg2IXiCD14iVX7AdZo1 h2 a").each((i, el) => {
        links[i] = $(el).attr("href");
      });
      $(".OgdwYG6KE2qthn9XQWFC").each((i, el) => {
        snippets[i] = $(el).text();
      });
      // console.log("titles:", titles);
      // console.log("links:", links);
      // console.log("snippets:", snippets);

      for (let i = 0; i < titles.length; i++) {
        parsedResults.push({
          count: i,
          title: titles[i],
          links: links[i],
          snippet: snippets[i]
        });
        justUrlResults.push(links[i])
      }

      // writing results to files
      exportResults(parsedResults, justUrlResults, fileName); // "DuckDuckGo");
    });       
  }


  ////////////////////////////////////////////////////////////////
  // CASE Bing:
  if (searchEngine.toLowerCase() == "bing") {
    console.log("BING!");

    // #b_results li h2 a - both links and link text
    // b_alogslug is the snippet!

    fs.readFile(basePath + file, 'utf8', (err, data) => {

      if (err) throw err;
      $ = cheerio.load(data);
          
      // Using Cheerio to find elements in page
      $("#b_results li h2 a").each((i, el) => {
        titles[i] = $(el).text();
      });  
      $("#b_results li h2 a").each(async (i, el) => {
        links[i] = $(el).attr("href");
      });
      $(".b_algoSlug").each((i, el) => {
        snippets[i] = $(el).text();
      });
      // console.log("titles:", titles);
      // console.log("links:", links);
      // console.log("snippets:", snippets);

      for (let i = 0; i < titles.length; i++) {
        parsedResults.push({
          count: i,
          title: titles[i],
          links: links[i],
          snippet: snippets[i]
        });
        justUrlResults.push(links[i])
      }

      // writing results to files
      exportResults(parsedResults, justUrlResults, fileName); // "Bing");
    });
  }


  ////////////////////////////////////////////////////////////////
  // CASE Yahoo!:
  if (searchEngine.toLowerCase() == "yahoo") {
    console.log("Yahoo!");

    // #b_results li h2 a - both links and link text
    // b_alogslug is the snippet!

    fs.readFile(basePath + file, 'utf8', (err, data) => {

      if (err) throw err;
      $ = cheerio.load(data);
          
      // Using Cheerio to find elements in page
      $(".compTitle h3 a.va-bot").each((i, el) => {
        titles[i] = $(el).text();
      });  
      $(".compTitle h3 a.va-bot").each((i, el) => {
        links[i] = $(el).attr("href");
      });
      $(".compText p").each((i, el) => {
        snippets[i] = $(el).text();
      });
      // console.log("titles:", titles);
      // console.log("links:", links);
      // console.log("snippets:", snippets);

      for (let i = 0; i < titles.length; i++) {
        parsedResults.push({
          count: i,
          title: titles[i],
          links: links[i],
          snippet: snippets[i]
        });
        justUrlResults.push(links[i])
      }

      // writing results to files
      exportResults(parsedResults, justUrlResults, fileName); // "Yahoo");
    });
  }


////////////////////////////////////////////////////////////////
  // CASE Yandex:
  if (searchEngine.toLowerCase() == "yandex") {
    console.log("Yandex");

    // #b_results li h2 a - both links and link text
    // b_alogslug is the snippet!

    fs.readFile(basePath + file, 'utf8', (err, data) => {

      if (err) throw err;
      $ = cheerio.load(data);
          
      // Using Cheerio to find elements in page
      $(".OrganicTitle-LinkText .OrganicTitleContentSpan").each((i, el) => {
        titles[i] = $(el).text();
      });  
      $(".OrganicTitle .OrganicTitle-Link").each((i, el) => {
        links[i] = $(el).attr("href");
      });
      $(".OrganicText .OrganicTextContentSpan").each((i, el) => {
        snippets[i] = $(el).text();
      });
      // console.log("titles:", titles);
      // console.log("links:", links);
      // console.log("snippets:", snippets);

      for (let i = 0; i < titles.length; i++) {
        parsedResults.push({
          count: i,
          title: titles[i],
          links: links[i],
          snippet: snippets[i]
        });
        justUrlResults.push(links[i])
      }

      // writing results to files
      exportResults(parsedResults, justUrlResults, fileName); // "Yahoo");
    });
  }  

}


///////////////////////////////////////////////
// save this to data.json
const exportResults = (parsedResults, justUrlResults, fileName) => {
  const time = getDateAndTime();
  const outputFile = `data_${time}_${fileName}.json`;
  const urlFile = `urls_${time}_${fileName}.txt`;

  // write data json file
  fs.writeFile(dataOutputDir + outputFile, JSON.stringify(parsedResults, null, 2), (err) => {
    if (err) { console.log(err) }
    console.log(chalk.yellow.bgBlue(`\n ${chalk.underline.bold(parsedResults.length)} Results exported successfully to ${chalk.underline.bold(outputFile)} \n`))
  })

  // write txt file with just the urls
  const writeStream = fs.createWriteStream(urlOutputDir + urlFile);
  writeStream.on('error', (err) => { console.log(err) });
  justUrlResults.forEach(value => writeStream.write(`${value}\n`));
  writeStream.on('finish', () => { console.log(`wrote all the array data to file ${urlOutputDir + urlFile}`); });
  writeStream.on('error', (err) => { console.error(`There is an error writing the file ${urlOutputDir + urlFile} => ${err}`) });
  writeStream.end();
}


///////////////////////////////////////////////
// format date and time for filename
const getDateAndTime = () => {
  const nowTime = new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
  const formatTime = nowTime.replace(/:/g, "-").slice(0,8);
  return `${(new Date().toJSON().slice(0,10))}__${formatTime}`;
}


// Processing args
// process.argv.forEach(function (val, index, array) { console.log(index + ': ' + val); });
const searchEngine = process.argv[2];
const file = process.argv[3];
// analyze
analyze(searchEngine, file);