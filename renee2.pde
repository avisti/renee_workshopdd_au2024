import processing.pdf.*;

String file1[];
String file2[];
PFont myFont;
int StringLength = 30;
int letterHeight = 11;


////////////////////////////////////////////////////////////////
void setup(){
  size(600, 1024);
  file1 = loadStrings("htmlLinks/urls_2024-04-24__12-57-27_FirefoxGoogle-html.txt");
  file2  = loadStrings("htmlLinks/urls_2024-04-24__12-58-12_FirefoxDuckduckgo-html.txt");
  // String[] fontList = PFont.list();
  // printArray(fontList);
  myFont = createFont("Arial", 32);
  textFont(myFont);
  
  // log file lengths
  println("there are " + file1.length + " lines in file1");
  println("there are " +file2.length + " lines in file2");

  beginRecord(PDF, "processingSketch.pdf");
}


////////////////////////////////////////////////////////////////
void draw(){
  background(255);
  // textFont(mono);
  fill(0);
  
  for (int i = 0 ; i < file1.length; i++) {
    // println(file1[i]);
    textSize(10);
    String file1Item = file1[i];
    
    if (file1Item.length() > StringLength) file1Item = file1Item.substring(0, StringLength)+"...";
    if (!file1Item.equals("")) {
      Boolean exists = checkConnection(file1[i], i, 0);
      if (exists==true) rect(200, i*letterHeight+15, 5, 5);
    }
    text(file1Item, 20, i*letterHeight+20);
    fill(0);
  }
    
  for (int i = 0 ; i < file2.length; i++) {
    //println(file2[i]);
    textSize(10);
   
    String file2Item = file2[i];
    if (file2Item.length() > StringLength) file2Item = file2Item.substring(0, StringLength)+"...";
    if (!file2Item.equals("")){
      Boolean exists = checkConnection(file2[i], i, 1);
      if (exists==true) rect(380, i*letterHeight+15, 5, 5);
    }
    text(file2Item, 400, i*letterHeight+20);
    fill(0);
  }
  
  endRecord();
}


////////////////////////////////////////////////////////////////
Boolean checkConnection(String value, int requestPos, int type){
  Boolean ret = false;
  String [] toCheck;
  if (type==0) { toCheck = file2; }
  else { toCheck = file1; }
  
  for (int i = 0 ; i < toCheck.length; i++) {
    if (toCheck[i].equals(value)){
      if (type==0){
        int startX = 205;
        int startY = requestPos*letterHeight+18;
        int endX = 380;
        int endY = i*letterHeight+18;
        line(startX,startY,endX,endY);
      }
      fill(255,0,0);
      ret = true;
    }else{
     
    }
  }
  return ret;
}
